package com.bizlinks.training.service.impl;

import org.springframework.stereotype.Service;

import com.bizlinks.training.service.MainService;

@Service
public class ServiceImpl implements MainService {

	@Override
	public String hello() {

		return "Hola definicion principal";
	}

}
