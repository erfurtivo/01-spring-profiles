package com.bizlinks.training;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bizlinks.training.service.MainService;

@RestController
public class Principal {

	@Autowired
	private MainService service;
	
	@GetMapping(produces = MediaType.TEXT_PLAIN_VALUE,value = "/hola")
	public String llamado() {
		return service.hello();
	}
	
	
}
